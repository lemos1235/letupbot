import os
import string
from http.client import RemoteDisconnected
from random import choice, sample

import requests
from requests.exceptions import ConnectionError, Timeout, HTTPError
from telegram import InlineKeyboardButton, InlineKeyboardMarkup

from ducky.main import Main, Tele, Crawler
from ducky.music import SongListSelector, SongFile, PlayListSelector, Song, Album, Artist, Playlist, User, SongList
from util import BotResult, SessionBase, load_config, conn_retry, UrlUpdateError, NoCopyRight, ButtonItem, \
    slice_keywords, escape_keywords, Singleton, get_sources_btn, progress_download, encrypted_request, userAgentList


class Netease(Main):
    def __init__(self):
        super().__init__(OwnCrawler(), Util())


class OwnCrawler(Crawler, metaclass=Singleton):

    def __init__(self):
        super().__init__()
        self.headers = {
            "Accept": "*/*",
            "Accept-Language": "zh-CN,zh;q=0.8,gl;q=0.6,zh-TW;q=0.4",
            "Connection": "keep-alive",
            "Content-Type": "application/x-www-form-urlencoded",
            "Referer": "http://music.163.com",
            "Host": "music.163.com",
            'Cookie': 'appver=1.5.2',  # cookie without linux, show the picUrl property
            'X-Real-IP': '183.232.231.173',
            'User-Agent': choice(userAgentList)
        }
        self.session = requests.Session()
        self.session.headers.update(self.headers)
        self.login_headers = self.headers.copy()
        self.device_cookie = "; appver=1.5.9; os=osx; channel=netease; osver=%E7%89%88%E6%9C%AC%2010.13.2%EF%BC%88%E7%89%88%E5%8F%B7%2017C88%EF%BC%89;MUSIC_U={};"
        self.login_headers["Cookie"] = self.device_cookie.format(load_config()['netease_token'])

    @staticmethod
    def dump_single_song(song, style=0):
        if style == 0:
            artist_list = []
            for ar in song['ar']:
                artist_id, artist_name = ar['id'], ar['name']
                artist_list.append(Artist(artist_id, artist_name))

            album_id, album_name, album_pic_id = song['al']['id'], song['al']['name'], song['al']['pic']
            if 'picUrl' in song['al']:
                album_pic_url = song['al']['picUrl']
                album = Album(album_id, album_name, album_pic_id, album_pic_url)
            else:
                album = Album(album_id, album_name, album_pic_id)
            song_id, song_name, song_duration, artists, album = song['id'], song['name'], song['dt'], artist_list, album
            song = Song(song_id, song_name, song_duration, artists, album)
            return song
        else:
            artist_list = []
            for ar in song['artists']:
                artist_id, artist_name = ar['id'], ar['name']
                artist_list.append(Artist(artist_id, artist_name))
            album_id, album_name, album_pic_id = song['album']['id'], song['album']['name'], song['album']['pic']
            if 'picUrl' in song['album']:
                album_pic_url = song['album']['picUrl']
                album = Album(album_id, album_name, album_pic_id, album_pic_url)
            else:
                album = Album(album_id, album_name, album_pic_id)
            song_id, song_name, song_duration, artists, album = song['id'], song['name'], song[
                'duration'], artist_list, album
            song = Song(song_id, song_name, song_duration, artists, album)
            return song

    @staticmethod
    def dump_songs(songs, style=0):
        song_list = []
        for song in songs:
            song = OwnCrawler.dump_single_song(song, style)
            song_list.append(song)
        return song_list

    def post_request(self, url, params, custom_session=None):
        """Send a post request.

        :return: a dict or raise Exception.
        """

        data = encrypted_request(params)

        resp = self.session.post(url, data=data, timeout=self.timeout,
                                 proxies=self.proxies)

        result = resp.json()
        if result['code'] != 200:
            self.logger.error('Return %s when try to post %s => %s',
                              result, url, params)
            raise ConnectionError(result)
        else:
            return result

    def search(self, search_content, search_type, page, pagesize=5):

        url = 'http://music.163.com/weapi/cloudsearch/get/web?csrf_token='
        params = {'s': search_content, 'type': search_type, 'offset': (page - 1) * pagesize,
                  'sub': 'false', 'limit': pagesize}
        result = self.post_request(url, params)
        return result

    @conn_retry(exce=(Timeout, ConnectionError, RemoteDisconnected))
    def search_song(self, song_name, page=1, pagesize=5):
        try:
            result = self.search(song_name, search_type=1, page=page, pagesize=pagesize)
            if 'songs' not in result['result'] or result['result']['songCount'] <= 0:
                self.logger.warning('Song %s not existed!', song_name)
                return BotResult(404, 'Song {} not existed.'.format(song_name))
            else:
                keyword, song_count, songs = song_name, result['result']['songCount'], result['result']['songs']
                songlist = SongList(keyword, song_count, OwnCrawler.dump_songs(songs))
                return BotResult(200, body=songlist)
        except HTTPError as er:
            self.logger.error('Search Song Error', exc_info=True)
            return BotResult(400, "HTTPError")

    @conn_retry(exce=(Timeout, ConnectionError))
    def search_playlist(self, playlist_id, page=1):
        url = 'http://music.163.com/weapi/v3/playlist/detail?csrf_token='
        csrf = ''
        params = {'id': playlist_id, 'offset': (page - 1) * 5, 'total': True,
                  'limit': 5, 'n': 1000, 'csrf_token': csrf}
        try:
            result = self.post_request(url, params)
            creator = User(result['playlist']['creator']['userId'], result['playlist']['creator']['nickname'])
            playlist_id, playlist_name, track_count, creator, songs = \
                result['playlist']['id'], result['playlist']['name'], result['playlist']['trackCount'], creator, \
                result['playlist']['tracks']
            playlist = Playlist(playlist_id, playlist_name, track_count, creator, OwnCrawler.dump_songs(songs))
            return BotResult(200, body=playlist)
        except HTTPError as er:
            self.logger.error('Return {0} when try to get {1} => {2}'.format(er, url, params), exc_info=True)
            return BotResult(400, "HTTPError")

    @conn_retry(exce=(Timeout, ConnectionError))
    def get_song_detail(self, song_id, quality=None):
        url = 'http://music.163.com/weapi/v3/song/detail'
        params = {'c': '[{{"id\": {}}}]'.format(song_id), 'ids': [song_id], 'csrf_token': ''}
        try:
            result = self.post_request(url, params)
            song = result['songs'][0]
            single_song = OwnCrawler.dump_single_song(song)
            # 获取 song_url
            if quality == 'highest':
                url = self.get_song_url(song_id, 999000)
            else:
                url = self.get_song_url(song_id)
            single_song.song_url = url
            # self.logger.debug('Get Song Detail songid=%s songname=%s songurl=%s', single_song.song_id,
            #                   single_song.song_name, single_song.song_url)
            return BotResult(200, body=single_song)
        except NoCopyRight as e:
            return BotResult(401, "HTTPError, no copyright!")
        except HTTPError as er:
            self.logger.error('Return {0} when try to get {1} => {2}'.format(er, url, params), exc_info=True)
            return BotResult(400, "HTTPError, connect!")

    @conn_retry(exce=(Timeout, ConnectionError, UrlUpdateError))
    def get_song_url(self, song_id, bit_rate=320000):

        url = 'http://music.163.com/weapi/song/enhance/player/url?csrf_token='
        csrf = ''
        params = {'ids': [song_id], 'br': bit_rate, 'csrf_token': csrf}

        resp = requests.session().post(url, data=encrypted_request(params), headers=self.login_headers,
                                       timeout=self.timeout)
        result = resp.json()

        if result['code'] != 200:
            self.logger.error('Return %s when try to get %s', result, url)
            raise HTTPError('Connect status is not 200!')
        else:
            song_url = result['data'][0]['url']  # download address
            # self.logger.debug('Get Song Url: songid=%s songurl=%s', song_id, song_url)
            if song_url is None:  # Taylor Swift's song is not available
                self.logger.warning(
                    'Song %s is not available due to copyright issue. => %s',
                    song_id, result)
                raise NoCopyRight('no copyright!')
            elif song_url.startswith('http://m7c.musics.126.net'):
                raise UrlUpdateError(song_url)
            else:
                return song_url

    @conn_retry(exce=(Timeout, ConnectionError))
    def get_personal_fm(self, user_id=10086):
        url = 'http://music.163.com/weapi/v1/radio/get?csrf_token='
        csrf = ''
        params = {'csrf_token': csrf}

        try:
            fm_headers = self.login_headers.copy()
            user_secret = SessionBase().fm_get(user_id)
            if user_secret:
                fm_headers["Cookie"] = self.device_cookie.format(user_secret)

            resp = requests.session().post(url, data=encrypted_request(params), headers=fm_headers,
                                           timeout=self.timeout)
            result = resp.json()

            if result['code'] != 200 or 'data' in result and len(result['data']) <= 0:
                self.logger.warning('Return %s when try to post %s => %s', result, url, params)
                return BotResult(400, 'error')
            else:
                result_list = list()
                result_songs = result['data']

                songlist = SongList('personalfm', 9999, OwnCrawler.dump_songs(result_songs, style=1))
                return BotResult(200, body=songlist)
        except HTTPError as er:
            self.logger.error('Return {0} when try to get {1} => {2}'.format(er, url, params), exc_info=True)
            return BotResult(400, "HTTPError")

    def write_file(self, songfile):
        progress_download(requests.session(), songfile)


class Util(Tele, metaclass=Singleton):
    def __init__(self):
        super().__init__()

    def get_songlist_selector(self, curpage, songlist):
        """
        生成可供选择的歌曲选择器
        :param curpage: 当前页数
        :param songlist: 歌曲列表🎵
        :return: 歌曲列表选择器
        """
        total_page = (songlist.track_count + 4) // 5
        title = 'ღ  Search  ‹ {0} ›  →  {1}/{2}'.format(songlist.keyword, curpage, total_page)
        return SongListSelector(title, curpage, total_page, songlist)

    def produce_songlist_panel(self, module_name, songlist_selector):
        button_list = []
        # 由于 tg 对 callback_data 字数限制，必须对关键词进行切片
        songlist_selector.songlist.keyword = slice_keywords(songlist_selector.songlist.keyword)

        for x in songlist_selector.songlist.songs:
            time_fmt = '{0}:{1:0>2d}'.format(int(x.song_duration / 1000 // 60), int(x.song_duration / 1000 % 60))
            button_list.append([
                InlineKeyboardButton(
                    text='{1} - {2}'.format(
                        time_fmt, x.song_name, ' / '.join(v.artist_name for v in x.artists)),
                    callback_data="{0}:{1}:{2}:{3}:{4}".format(module_name, ButtonItem.TYPE_SONGLIST,
                                                               ButtonItem.OPERATE_SEND,
                                                               x.song_id, 0)
                )
            ])

        button_list.append(get_sources_btn())

        if songlist_selector.total_page == 1:
            pass
        elif songlist_selector.cur_page == 1:
            button_list.append([
                InlineKeyboardButton(
                    text='»',
                    callback_data="{0}:{1}:{2}:{3}:{4}".format(module_name, ButtonItem.TYPE_SONGLIST,
                                                               ButtonItem.OPERATE_PAGE_DOWN,
                                                               songlist_selector.songlist.keyword,
                                                               songlist_selector.cur_page)
                )
            ])
        elif songlist_selector.cur_page == songlist_selector.total_page:
            button_list.append([
                InlineKeyboardButton(
                    text='«',
                    callback_data="{0}:{1}:{2}:{3}:{4}".format(module_name, ButtonItem.TYPE_SONGLIST,
                                                               ButtonItem.OPERATE_PAGE_UP,
                                                               songlist_selector.songlist.keyword,
                                                               songlist_selector.cur_page)
                )
            ])
        else:
            button_list.append([
                InlineKeyboardButton(
                    text='«',
                    callback_data="{0}:{1}:{2}:{3}:{4}".format(module_name, ButtonItem.TYPE_SONGLIST,
                                                               ButtonItem.OPERATE_PAGE_UP,
                                                               songlist_selector.songlist.keyword,
                                                               songlist_selector.cur_page)
                ),
                InlineKeyboardButton(
                    text='»',
                    callback_data="{0}:{1}:{2}:{3}:{4}".format(module_name, ButtonItem.TYPE_SONGLIST,
                                                               ButtonItem.OPERATE_PAGE_DOWN,
                                                               songlist_selector.songlist.keyword,
                                                               songlist_selector.cur_page)
                )
            ])

        l = "{0}:{1}:{2}:{3}:{4}".format(module_name, ButtonItem.TYPE_SONGLIST, ButtonItem.OPERATE_PAGE_DOWN,
                                         songlist_selector.songlist.keyword,
                                         songlist_selector.cur_page)
        # print(len(l.encode('utf-8')), "xxxxxxxxxxxxxxxxxxxxxx")
        return {'text': songlist_selector.title, 'reply_markup': InlineKeyboardMarkup(button_list)}

    def produce_fm_songlist_panel(self, module_name, songlist):
        button_list = []
        personal_fm_title = '🔊 Radio'

        for x in songlist.songs:
            button_list.append([
                InlineKeyboardButton(
                    text='{0} - {1}'.format(
                        x.song_name, ' / '.join(v.artist_name for v in x.artists)),
                    callback_data="{0}:{1}:{2}:{3}:{4}".format(module_name, ButtonItem.TYPE_PERSONAL_FM,
                                                               ButtonItem.OPERATE_SEND,
                                                               x.song_id, 0)
                )
            ])

        button_list.append([
            InlineKeyboardButton(
                text='next »',
                callback_data="{0}:{1}:{2}:{3}:{4}".format(module_name, ButtonItem.TYPE_PERSONAL_FM,
                                                           ButtonItem.OPERATE_PAGE_DOWN,
                                                           "next", 10000)
            )
        ])

        return {'text': personal_fm_title, 'reply_markup': InlineKeyboardMarkup(button_list)}

    def get_playlist_selector(self, curpage, playlist):
        total_page = (playlist.track_count + 4) // 5
        title = '{0}   \n by  {1}  ({2})    🔖'.format(
            playlist.playlist_name, playlist.creator.username, playlist.track_count)

        # 分页处理
        start = curpage * 5 - 5
        playlist.songs = playlist.songs[start:start + 5]

        return PlayListSelector(title, curpage, total_page, playlist)

    def produce_playlist_panel(self, module_name, playlist_selector):
        button_list = []

        for x in playlist_selector.playlist.songs:
            time_fmt = '{0}:{1:0>2d}'.format(int(x.song_duration / 1000 // 60), int(x.song_duration / 1000 % 60))
            button_list.append([
                InlineKeyboardButton(
                    text='{1} - {2}'.format(
                        time_fmt, x.song_name, ' / '.join(v.artist_name for v in x.artists)),
                    callback_data="{0}:{1}:{2}:{3}:{4}".format(module_name, ButtonItem.TYPE_PLAYLIST,
                                                               ButtonItem.OPERATE_SEND,
                                                               x.song_id, 0)
                )
            ])

        if playlist_selector.total_page == 1:
            pass
        elif playlist_selector.cur_page == 1:
            button_list.append([
                InlineKeyboardButton(
                    text='»',
                    callback_data="{0}:{1}:{2}:{3}:{4}".format(module_name, ButtonItem.TYPE_PLAYLIST,
                                                               ButtonItem.OPERATE_PAGE_DOWN,
                                                               playlist_selector.playlist.playlist_id,
                                                               playlist_selector.cur_page)
                )
            ])
        elif playlist_selector.cur_page == playlist_selector.total_page:
            button_list.append([
                InlineKeyboardButton(
                    text='«',
                    callback_data="{0}:{1}:{2}:{3}:{4}".format(module_name, ButtonItem.TYPE_PLAYLIST,
                                                               ButtonItem.OPERATE_PAGE_UP,
                                                               playlist_selector.playlist.playlist_id,
                                                               playlist_selector.cur_page)
                )
            ])
        else:
            button_list.append([
                InlineKeyboardButton(
                    text='«',
                    callback_data="{0}:{1}:{2}:{3}:{4}".format(module_name, ButtonItem.TYPE_PLAYLIST,
                                                               ButtonItem.OPERATE_PAGE_UP,
                                                               playlist_selector.playlist.playlist_id,
                                                               playlist_selector.cur_page)
                ),
                InlineKeyboardButton(
                    text='»',
                    callback_data="{0}:{1}:{2}:{3}:{4}".format(module_name, ButtonItem.TYPE_PLAYLIST,
                                                               ButtonItem.OPERATE_PAGE_DOWN,
                                                               playlist_selector.playlist.playlist_id,
                                                               playlist_selector.cur_page)
                )
            ])

        return {'text': playlist_selector.title, 'reply_markup': InlineKeyboardMarkup(button_list)}

    def get_songfile(self, song):
        song.song_name = slice_keywords(song.song_name, 200, 10)  # 对文件名长度限制一般性处理
        audio_ext = os.path.splitext(song.song_url)[1]
        file_name = r'{0} - {1}{2}'.format(
            song.song_name, ' & '.join(v.artist_name for v in song.artists), audio_ext)
        file_name = escape_keywords(file_name, '/', ':')
        file_path = os.path.join(self.tmp_folder, file_name)
        file_url = song.song_url
        if os.path.exists(file_path):
            salt = ''.join(sample(string.ascii_letters + string.digits, 12))
            new_file_name = r'{0} - {1}{3}{2}'.format(
                song.song_name, ' & '.join(v.artist_name for v in song.artists), audio_ext, salt)
            file_path = os.path.join(self.tmp_folder, new_file_name)
        file_stream = open(file_path, 'wb+')
        file_ext = 'audio/{}'.format(audio_ext[1:])
        song = song
        return SongFile(file_name, file_path, file_url, file_stream, file_ext, song)
