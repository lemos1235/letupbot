import base64
import json
import logging
import time
from io import BytesIO

import requests
from requests.exceptions import ConnectionError, Timeout
from telegram import TelegramError, ParseMode, ChatAction

from util import BotResult, conn_retry, load_config
from util import telegram_timeout, escape_keywords, telegram_error


class Animation(object):

    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.crawler = Crawler()

    @property
    def unique_id(self):
        return '{}_c'.format(self.__class__.__name__).lower()

    def search_animation(self, bot, update):
        try:
            _file = self.get_file(bot, update.message)
            if _file:
                result = self.search_image(_file)
                if result.status == 200:
                    anime_file = result.get_body()
                    self.send_animation_info_preview(bot, update, anime_file)
                elif result.status == 400 or result.status == 404:
                    self.logger.warning(result.get_msg())
        except (ValueError, TelegramError) as e:
            self.logger.error(e)

    @telegram_timeout
    @telegram_error
    def get_file(self, bot, update_msg):
        _file = None
        if update_msg.photo:
            _file = bot.get_file(update_msg.photo[-1].file_id)
        elif update_msg.sticker:
            _file = bot.get_file(update_msg.sticker.file_id)
        elif update_msg.document and update_msg.document.thumb:
            _file = bot.get_file(update_msg.document.thumb.file_id)
        return _file

    def search_image(self, get_file):
        self.logger.debug('Search anime file_id=%s', get_file.file_id)
        pic_url = get_file.file_path
        pic_content = requests.get(pic_url).content
        image_base64 = base64.b64encode(pic_content)
        bot_result = self.crawler.get_animation_detail(image_base64)
        return bot_result

    @telegram_timeout
    @telegram_error
    def send_animation_info_preview(self, bot, update, anime):
        self.logger.debug('Send anime preview anime_name=%s', anime.anime_name)
        if anime.timeline >= 3600:
            time_format = time.strftime("%H时%M分%S秒", time.gmtime(anime.timeline))
        elif anime.timeline >= 60:
            time_format = time.strftime("%M分%S秒", time.gmtime(anime.timeline))
        else:
            time_format = time.strftime("%S秒", time.gmtime(anime.timeline))

        if anime.episode == 0:
            sformat = "__{0}__\n剧场版  {2}\n🔖 {3:.1%}"
        else:
            sformat = "__{0}__\n{1:0>2d}话  {2}\n📌 {3:.1%}"
        # anime_url = 'https://anilist.co/anime/{}'.format(anime.anilist_id)
        anime.anime_name = escape_keywords(anime.anime_name, '`_[]*', ' ')
        anime_relative_info = sformat.format(anime.anime_name, anime.episode, time_format, anime.similarity)

        result = self.crawler.get_lvideo_url(anime)
        if result.status == 200:
            content = result.get_body()
            with BytesIO(content) as f:
                bot.send_chat_action(update.message.chat.id, action=ChatAction.UPLOAD_VIDEO)
                update.message.reply_video(f, timeout=55, caption=anime_relative_info, parse_mode=ParseMode.MARKDOWN,
                                           supports_streaming=True, quote=True)
        else:
            self.logger.warning("Failed to get Anime video url")


class AnimationFile(object):
    def __init__(self, anilist_id, filename, anime_name, episode, timeline, similarity, tokenthumb=None):
        self.anilist_id = anilist_id
        self.filename = filename
        self.anime_name = anime_name
        self.episode = episode
        self.timeline = timeline
        self.similarity = similarity
        self.tokenthumb = tokenthumb

    def convert_to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__, ensure_ascii=False,
                          sort_keys=True, indent=4)


class Crawler(object):
    def __init__(self, timeout=55, proxy=None):
        self.logger = logging.getLogger(__name__)
        self.headers = {
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
            "Host": "whatanime.ga"
        }
        self.timeout = timeout
        self.proxies = {'http': proxy, 'https': proxy}
        self.session = requests.session()
        self.session.headers.update(self.headers)

    def post_request(self, url, params, custom_session=None):
        """Send a post request.

        :return: a dict or raise Exception.
        """
        if not custom_session:
            resp = self.session.post(url, data=params, timeout=self.timeout,
                                     proxies=self.proxies)
        else:
            resp = custom_session.post(url, data=params, timeout=self.timeout,
                                       proxies=self.proxies)
        result = resp.json()
        return result

    @conn_retry(exce=Timeout)
    def get_animation_detail(self, file_content):
        try:
            url = "https://whatanime.ga/api/search?token={}".format(load_config()['animation_token'])
            data = {
                "image": file_content
            }
            result = self.post_request(url, data)
            if not result.get("docs"):
                return BotResult(404, "anime not found: {}".format(result))
            else:
                episode_num = result["docs"][0]["episode"]
                if not episode_num:
                    episode_num = 0
                anilist_id, filename, anime_name, episode, timeline, similarity = \
                    result["docs"][0]["anilist_id"], result["docs"][0]["filename"], result["docs"][0]["title_chinese"], \
                    episode_num, result["docs"][0]["at"], result["docs"][0]["similarity"]
                tokenthumb = result["docs"][0]["tokenthumb"] if result["docs"][0].get("tokenthumb") else None
                anime = AnimationFile(anilist_id, filename, anime_name, episode, timeline, similarity,
                                      tokenthumb=tokenthumb)
                return BotResult(200, body=anime)
        except ConnectionError as e:
            self.logger.error(e)
            return BotResult(400, "Failed to get the picture detail. {}".format(e))

    @conn_retry(exce=(Timeout, ConnectionError))
    def get_lvideo_url(self, anime_file):
        url = "https://whatanime.ga/preview.php"
        params = {
            "anilist_id": anime_file.anilist_id,
            "file": anime_file.filename,
            "t": anime_file.timeline,
            "token": anime_file.tokenthumb
        }
        resp = self.session.get(url, params=params, timeout=self.timeout, proxies=self.proxies)
        return BotResult(200, body=resp.content)
