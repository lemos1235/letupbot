import logging
import os
from abc import abstractmethod
from threading import Event, Thread

from telegram import TelegramError, ChatAction
from telegram.error import BadRequest, ChatMigrated

from util import ButtonItem, selector_cancel, DataCache, telegram_timeout, telegram_error, \
    SessionBase


class Main(object):

    def __init__(self, crawler=None, tele=None):
        self.logger = logging.getLogger(__name__)
        self.crawler = crawler
        self.tele = tele
        self.data_cache = DataCache()

    @property
    def unique_id(self):
        return '{}_c'.format(self.__class__.__name__).lower()

    def find_music(self, bot, update, kw):
        self.logger.debug('Search Music searchKey=%s', kw)

        self.songlist_turning(bot, update, kw, first=True)

    def find_playlist(self, bot, update, playlist_id):
        try:
            self.playlist_turning(bot, update, playlist_id, first=True)
        except IndexError:
            self.logger.warning('Could not find the playlist', exc_info=True)

    def find_personalfm(self, bot, update):
        self.personal_fmturning(bot, update, first=True)

    def analyze_music_callback(self, bot, update):
        """监听响应的内容，取消、翻页或者下载
        如果为取消，则直接删除选择列表
        如果为翻页，则修改选择列表并进行翻页
        如果为发送，则获取 music_id 并生成 NeteaseMusic。然后，加载-获取歌曲url，发送音乐文件，删除上一条信息
        :return:
        """
        query = update.callback_query
        button_item = ButtonItem.parse_data(query.data)
        self.handle_callback(bot, query, button_item)

    @telegram_timeout
    @telegram_error
    def songlist_turning(self, bot, query, kw, page=1, first=False):
        bot_result = self.crawler.search_song(kw, page)
        if bot_result.status == 200:
            selector = self.tele.get_songlist_selector(page, bot_result.get_body())
            panel = self.tele.produce_songlist_panel(self.unique_id, selector)
            if first:
                query.message.reply_text(text=panel['text'], reply_markup=panel['reply_markup'])
            else:
                query.message.edit_text(text=panel['text'], reply_markup=panel['reply_markup'])
        elif bot_result.status == 404:
            text = "?"
            query.message.reply_text(text=text, quote=True)

    @telegram_timeout
    @telegram_error
    def playlist_turning(self, bot, query, playlist_id, page=1, first=False):
        bot_result = self.crawler.search_playlist(playlist_id, page)
        if bot_result.status == 400:
            # text = "?"
            # query.message.reply_text(text=text, timeout=40)
            pass
        elif bot_result.status == 200:
            selector = self.tele.get_playlist_selector(page, bot_result.get_body())
            panel = self.tele.produce_playlist_panel(self.unique_id, selector)
            if first:
                query.message.reply_text(text=panel['text'], reply_markup=panel['reply_markup'])
            else:
                query.message.edit_text(text=panel['text'], reply_markup=panel['reply_markup'])

    @telegram_timeout
    @telegram_error
    def personal_fmturning(self, bot, query, first=False):
        bot_result = self.crawler.get_personal_fm(user_id=query.message.chat.id)
        if bot_result.status == 200:
            panel = self.tele.produce_fm_songlist_panel(self.unique_id, bot_result.get_body())
            if first:
                query.message.reply_text(text=panel['text'], reply_markup=panel['reply_markup'])
            else:
                query.message.edit_text(text=panel['text'], reply_markup=panel['reply_markup'])
        elif bot_result.status == 400:
            pass
            # query.message.reply_text(text='?', quote=True)
        elif bot_result.status == 500:
            self.logger.warning('500 error')

    @staticmethod
    def delete_panel(bot, query):
        bot.answerCallbackQuery(query.id, text="loading..", show_alert=False)
        try:
            if query.message:
                if not Event().wait(1):
                    if query.message:
                        query.message.delete()
        except BadRequest as e:
            pass

    def deliver_music(self, bot, query, song_id, delete=False):
        self.logger.debug('Deliver musics song_id=%s', song_id)
        if delete:
            Thread(target=self.delete_panel, args=(bot, query)).start()

        quality = SessionBase().uget(query.message.chat.id, 'quality_c')

        bot_result = self.crawler.get_song_detail(song_id, quality)

        if bot_result.status == 200:
            song = bot_result.get_body()
            songfile = self.tele.get_songfile(song)
            try:
                self.pull_file(bot, query, songfile)
            except TelegramError as er:
                self.logger.error('Telegram error: like more than 50M %s', er)
        elif bot_result.status == 401:
            text = "©"
            query.message.reply_text(text=text, quote=False)
        elif bot_result.status == 400:
            pass

    def handle_callback(self, bot, query, btn):
        if btn.button_type == ButtonItem.TYPE_SONGLIST:
            if btn.button_operate == ButtonItem.OPERATE_SEND:
                self.deliver_music(bot, query, btn.item_id, delete=True)
            elif btn.button_operate == ButtonItem.OPERATE_PAGE_DOWN:
                self.songlist_turning(bot, query, btn.item_id, btn.page + 1)
            elif btn.button_operate == ButtonItem.OPERATE_PAGE_UP:
                self.songlist_turning(bot, query, btn.item_id, btn.page - 1)
        elif btn.button_type == ButtonItem.TYPE_PLAYLIST:
            if btn.button_operate == ButtonItem.OPERATE_SEND:
                self.deliver_music(bot, query, btn.item_id, delete=False)
            elif btn.button_operate == ButtonItem.OPERATE_PAGE_DOWN:
                self.playlist_turning(bot, query, btn.item_id, btn.page + 1)
            elif btn.button_operate == ButtonItem.OPERATE_PAGE_UP:
                self.playlist_turning(bot, query, btn.item_id, btn.page - 1)
        elif btn.button_type == ButtonItem.TYPE_PERSONAL_FM:
            if btn.button_operate == ButtonItem.OPERATE_PAGE_DOWN:
                self.personal_fmturning(bot, query)
            elif btn.button_operate == ButtonItem.OPERATE_SEND:
                self.deliver_music(bot, query, btn.item_id, delete=False)
        elif btn.button_operate == ButtonItem.OPERATE_CANCEL:
            selector_cancel(bot, query)

    @telegram_timeout
    @telegram_error
    def pull_file(self, bot, query, songfile, ):
        self.logger.debug('Songfile downloading songname=%s songid=%s', songfile.song.song_name, songfile.song.song_id)

        try:
            # Pull from DataCache
            _file_id = self.data_cache.song_get(self.unique_id, songfile.song.song_id)
            if _file_id and songfile.file_ext == 'audio/mp3':
                songfile.tg_file_id = _file_id
            else:
                self.crawler.write_file(songfile)
                songfile.set_id3tags(songfile.song.song_name, list(v.artist_name for v in songfile.song.artists),
                                     song_album=songfile.song.album.album_name,
                                     album_pic_url=songfile.song.album.album_pic_url)

            self.push_file(bot, query, songfile)
        except OSError as e:
            self.logger.warning('download error, %s could not read file', e)
        finally:
            if os.path.exists(songfile.file_path):
                os.remove(songfile.file_path)
            if not songfile.file_stream.closed:
                songfile.file_stream.close()

    @telegram_timeout
    def push_file(self, bot, query, songfile):
        audio_msg = None
        try:
            # bot.send_chat_action(query.message.chat.id, action=ChatAction.UPLOAD_AUDIO)
            if songfile.tg_file_id:
                # Here is a limit 10 MB max size for photos, 50 MB for other files.

                audio_msg = query.message.reply_audio(audio=songfile.tg_file_id, timeout=55, quote=False)
            else:
                # limit 10 MB max size for photos, 50 MB for other files.
                audio_msg = query.message.reply_audio(audio=open(songfile.file_path, 'rb'),
                                                      caption='',
                                                      duration=songfile.song.song_duration / 1000,
                                                      title=songfile.song.song_name,
                                                      performer=' / '.join(
                                                          v.artist_name for v in songfile.song.artists),
                                                      timeout=160, quote=False)
                if songfile.file_ext == 'audio/mp3':
                    self.data_cache.song_set(self.unique_id, songfile.song.song_id, audio_msg.audio.file_id)
                    self.logger.debug("FILE 「%s」 has been added DataCache Storage", songfile.song.song_name)

        except (BadRequest, ChatMigrated) as es:
            self.logger.warning('Telegram error %s', es)
            if audio_msg:
                audio_msg.delete()
            # Remove the broken file_id from Database Cache and forbid to be stored twice for 1 day
            self.data_cache.song_delete(self.unique_id, songfile.song.song_id)
            self.logger.error("File 「%s」 failed to send. Maybe beyound the tg file size limit(50M)",
                              songfile.song.song_name)
            text = "??"
            query.message.reply_text(text=text, quote=False)

    def group_music_search(self, bot, update, kw):
        bot_result = self.crawler.search_song(kw)
        if bot_result.status == 200:
            songlist = bot_result.get_body()
            if songlist.songs:
                song_id = songlist.songs[0].song_id
                self.group_single_music(bot, update, song_id)

    @telegram_timeout
    def group_single_music(self, bot, update, song_id):
        songfile = None
        try:
            bot_result = self.crawler.get_song_detail(song_id)
            if bot_result.status == 200:
                song = bot_result.get_body()
                songfile = self.tele.get_songfile(song)

                # Pull from DataCache
                _file_id = self.data_cache.song_get(self.unique_id, songfile.song.song_id)
                if _file_id and songfile.file_ext == 'audio/mp3':
                    songfile.tg_file_id = _file_id
                else:
                    self.crawler.write_file(songfile)
                    songfile.set_id3tags(songfile.song.song_name, list(v.artist_name for v in songfile.song.artists),
                                         song_album=songfile.song.album.album_name,
                                         album_pic_url=songfile.song.album.album_pic_url)

                bot.send_chat_action(update.message.chat.id, action=ChatAction.UPLOAD_AUDIO)

                if songfile.tg_file_id:
                    # Here is a limit 10 MB max size for photos, 50 MB for other files.
                    update.message.reply_audio(audio=songfile.tg_file_id, caption='',
                                               disable_notification=True)
                else:
                    send_msg = update.message.reply_audio(audio=open(songfile.file_path, 'rb'), caption='',
                                                          duration=songfile.song.song_duration / 1000,
                                                          title=songfile.song.song_name,
                                                          performer=' / '.join(
                                                              v.artist_name for v in songfile.song.artists),
                                                          timeout=55, disable_notification=True)
                    # Push into DataCache
                    if songfile.file_ext == 'audio/mp3':
                        self.data_cache.song_set(self.unique_id, songfile.song.song_id, send_msg.audio.file_id)
                        self.logger.debug("FILE 「%s」 has been added DataCache Storage", songfile.song.song_name)
        except (BadRequest, ChatMigrated) as es:
            self.logger.warning('Telegram error %s', 'BadRequest')
            # Remove the broken file_id from Database Cache and forbid to be stored twice for 1 day
            if songfile:
                self.data_cache.song_delete(self.unique_id, songfile.song.song_id)


class Crawler(object):
    def __init__(self, timeout=10, proxy=None):
        self.timeout = timeout
        self.proxies = {'http': proxy, 'https': proxy}
        self.logger = logging.getLogger(__name__)

    @staticmethod
    def dump_single_song(song):
        pass

    @staticmethod
    def dump_songs(songs):
        pass

    def get_request(self, url, params=None, custom_session=None):
        pass

    def post_request(self, url, params, custom_session=None):
        pass

    def search(self, search_content, search_type, page):
        pass

    def search_song(self, song_name, page=1):
        pass

    def search_playlist(self, playlist_id, page=1):
        pass

    def search_songtop(self, search_type, page=1):
        pass

    @abstractmethod
    def get_song_detail(self, song_id, quality=None):
        pass

    def get_song_url(self, song_id):
        pass

    def write_file(self, songfile):
        pass

    def login(self, username, password):
        pass


class Tele(object):
    def __init__(self):
        self.logger = logging.getLogger(__name__)

        self.tmp_folder = "/tmp"

    def get_songlist_selector(self, curpage, songlist):
        pass

    def produce_songlist_panel(self, module_name, songlist_selector):
        pass

    def produce_fm_songlist_panel(self, module_name, songlist_selector):
        pass

    def get_playlist_selector(self, curpage, playlist):
        pass

    def produce_playlist_panel(self, module_name, playlist_selector):
        pass

    def get_toplist_selector(self, curpage, toplist):
        pass

    def produce_toplist_panel(self, module_name, toplist_selector):
        pass

    def get_songfile(self, song):
        pass
