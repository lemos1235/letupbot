import json
import math
import os
import re
import string
from http.client import RemoteDisconnected
from random import choice, sample, random as rand

import requests
from requests.exceptions import ConnectionError, Timeout, HTTPError
from telegram import InlineKeyboardButton, InlineKeyboardMarkup

from ducky.main import Main, Tele, Crawler
from ducky.music import SongListSelector, SongFile, Song, Album, Artist, SongList
from util import ButtonItem, slice_keywords, escape_keywords, Singleton, BotResult, conn_retry, get_sources_btn, \
    userAgentList, progress_download


class Tencent(Main):
    def __init__(self):
        super().__init__(OwnCrawler(), Util())


class OwnCrawler(Crawler, metaclass=Singleton):

    def __init__(self):
        super().__init__()
        self.headers = {
            'Accept': '*/*',
            'Accept-Encoding': 'gzip,deflate,sdch',
            'Accept-Language': 'zh-CN,zh;q=0.8,gl;q=0.6,zh-TW;q=0.4',
            'Connection': 'keep-alive',
            'Content-Type': 'application/x-www-form-urlencoded',
            'Referer': 'https://y.qq.com/portal/search.html',
            'Host': 'c.y.qq.com',
            'X-Real-IP': '117.185.116.152',
            'User-Agent': choice(userAgentList)
        }
        self.session = requests.session()
        self.session.headers.update(self.headers)
        self.download_session = requests.session()

    @staticmethod
    def dump_single_song(song, mode=0):
        artist_list = []
        for ar in song['singer']:
            artist_id, artist_name = ar['mid'], ar['name']
            artist_list.append(Artist(artist_id, artist_name))

        album_id, album_name = song['album']['mid'], song['album']['name']
        album = Album(album_id, album_name)

        song_id, song_name, song_duration, artists, album = \
            song['mid'], song['name'], song['interval'], artist_list, album
        song = Song(song_id, song_name, song_duration, artists, album)
        return song

    @staticmethod
    def dump_songs(songs, mode=0):
        song_list = []
        for song in songs:
            song = OwnCrawler.dump_single_song(song, mode)
            song_list.append(song)
        return song_list

    def get_request(self, url, params=None, custom_session=None, callback=None):
        """Send a get request.

        warning: old api.
        :return: a dict or raise Exception.
        """
        if not custom_session:
            resp = self.session.get(url, params=params, timeout=self.timeout,
                                    proxies=self.proxies)
        else:
            resp = custom_session.get(url, params=params, timeout=self.timeout,
                                      proxies=self.proxies)
        if callback:
            regex_str = r'{}\((.+)\)'.format(callback)
            data = re.match(regex_str, resp.text).group(1)
            return json.loads(data)
        else:
            result = resp.json()
            return result

    def parse_guid(self, guid):
        """
        解析 url的关键参数
        :return: {'code':0, 'sip': [], ... , 'key': ''}
        """
        url = "https://c.y.qq.com/base/fcgi-bin/fcg_musicexpress.fcg"
        payload = {
            'guid': guid,
            'json': 3,
            'format': 'json'
        }
        result = self.get_request(url, payload)
        return result

    @conn_retry(exce=(Timeout, ConnectionError, RemoteDisconnected))
    def search_song(self, song_name, page=1, pagesize=5):
        url = "https://c.y.qq.com/soso/fcgi-bin/client_search_cp"
        payload = {
            'w': song_name,
            'n': pagesize,
            'p': page,
            'aggr': 1,
            'lossless': 1,
            'cr': '1',
            'jsonpCallback': 'callback',
            'new_json': 1,
            'format': 'json'
        }
        try:
            result = self.get_request(url, payload)
            if result['code'] != 0:
                self.logger.warning('Song %s search failed! url=%s result=%s', song_name, url, result)
                return BotResult(400, 'Song {} search failed.'.format(song_name))
            if result['data']['song']['totalnum'] <= 0:
                self.logger.warning('Song %s not existed!', song_name)
                return BotResult(404, 'Song {} not existed.'.format(song_name))
            else:
                keyword, song_count, songs = song_name, result['data']['song']['totalnum'], result['data']['song'][
                    'list']
                songlist = SongList(keyword, song_count, OwnCrawler.dump_songs(songs))
                return BotResult(200, body=songlist)
        except HTTPError as er:
            self.logger.error('Return {0} when try to get {1} => {2}'.format(er, url, payload), exc_info=True)
            return BotResult(400, "HTTPError")

    @conn_retry(exce=(Timeout, ConnectionError))
    def get_song_detail(self, song_id, quality=None):
        url = 'http://c.y.qq.com/v8/fcg-bin/fcg_play_single_song.fcg'
        payload = {
            'songmid': song_id,
            'format': 'json'
        }
        try:
            result = self.get_request(url, payload)
            song = result['data'][0]
            single_song = OwnCrawler.dump_single_song(song)
            file = result['data'][0]['file']
            type_o = None
            if file['size_320mp3']:
                type_o = ('M800', 'mp3')
            elif file['size_128mp3']:
                type_o = ('M500', 'mp3')
            if type_o:
                url = self.get_song_url(song_id, type_o)
                single_song.song_url = url
                return BotResult(200, body=single_song)
            else:
                return BotResult(401, "HTTPError, no copyright!")
        except HTTPError as er:
            self.logger.error('Return {0} when try to get {1} => {2}'.format(er, url, payload), exc_info=True)
            return BotResult(400, "HTTPError")

    def get_song_url(self, song_id, type_o=None):
        guid = math.floor(rand() * 1000000000)
        secret_token = self.parse_guid(guid)
        base, prefix, extension, vkey, guid, fromtag = \
            secret_token['sip'][0], type_o[0], type_o[1], secret_token['key'], guid, 30
        song_url = "{0}{1}{2}.{3}?vkey={4}&guid={5}&fromtag={6}".format(
            base, prefix, song_id, extension, vkey, guid, fromtag)
        return song_url

    def write_file(self, songfile):
        progress_download(self.download_session, songfile)


class Util(Tele, metaclass=Singleton):
    def __init__(self):
        super().__init__()

    def get_songlist_selector(self, curpage, songlist):
        """
                生成可供选择的歌曲选择器
                :param curpage: 当前页数
                :param songlist: 歌曲列表🎵
                :return: 歌曲列表选择器
                """
        total_page = (songlist.track_count + 4) // 5
        title = '⌥  Search  ‹ {0} ›  →  {1}/{2}'.format(songlist.keyword, curpage, total_page)
        return SongListSelector(title, curpage, total_page, songlist)

    def produce_songlist_panel(self, module_name, songlist_selector):
        button_list = []
        # 由于 tg 对 callback_data 字数限制，必须对关键词进行切片
        songlist_selector.songlist.keyword = slice_keywords(songlist_selector.songlist.keyword)

        for x in songlist_selector.songlist.songs:
            time_fmt = '{0}:{1:0>2d}'.format(int(x.song_duration // 60), int(x.song_duration % 60))
            button_list.append([
                InlineKeyboardButton(
                    text='{1} - {2}'.format(
                        time_fmt, x.song_name, ' / '.join(v.artist_name for v in x.artists)),
                    callback_data="{0}:{1}:{2}:{3}:{4}".format(module_name, ButtonItem.TYPE_SONGLIST,
                                                               ButtonItem.OPERATE_SEND,
                                                               x.song_id, 0)
                )
            ])

        button_list.append(get_sources_btn())

        if songlist_selector.total_page == 1:
            pass
        elif songlist_selector.cur_page == 1:
            button_list.append([
                InlineKeyboardButton(
                    text='»',
                    callback_data="{0}:{1}:{2}:{3}:{4}".format(module_name, ButtonItem.TYPE_SONGLIST,
                                                               ButtonItem.OPERATE_PAGE_DOWN,
                                                               songlist_selector.songlist.keyword,
                                                               songlist_selector.cur_page)
                )
            ])
        elif songlist_selector.cur_page == songlist_selector.total_page:
            button_list.append([
                InlineKeyboardButton(
                    text='«',
                    callback_data="{0}:{1}:{2}:{3}:{4}".format(module_name, ButtonItem.TYPE_SONGLIST,
                                                               ButtonItem.OPERATE_PAGE_UP,
                                                               songlist_selector.songlist.keyword,
                                                               songlist_selector.cur_page)
                )
            ])
        else:
            button_list.append([
                InlineKeyboardButton(
                    text='«',
                    callback_data="{0}:{1}:{2}:{3}:{4}".format(module_name, ButtonItem.TYPE_SONGLIST,
                                                               ButtonItem.OPERATE_PAGE_UP,
                                                               songlist_selector.songlist.keyword,
                                                               songlist_selector.cur_page)
                ),
                InlineKeyboardButton(
                    text='»',
                    callback_data="{0}:{1}:{2}:{3}:{4}".format(module_name, ButtonItem.TYPE_SONGLIST,
                                                               ButtonItem.OPERATE_PAGE_DOWN,
                                                               songlist_selector.songlist.keyword,
                                                               songlist_selector.cur_page)
                )
            ])

        return {'text': songlist_selector.title, 'reply_markup': InlineKeyboardMarkup(button_list)}

    def get_songfile(self, song):
        song.song_name = slice_keywords(song.song_name, 200, 10)  # 对文件名长度限制一般性处理
        start = song.song_url.rindex('.')
        end = song.song_url.rindex('?')
        audio_ext = song.song_url[start: end]
        file_name = r'{0} - {1}{2}'.format(
            song.song_name, ' & '.join(v.artist_name for v in song.artists), audio_ext)
        file_name = escape_keywords(file_name, '/', ':')
        file_path = os.path.join(self.tmp_folder, file_name)
        file_url = song.song_url
        if os.path.exists(file_path):
            salt = ''.join(sample(string.ascii_letters + string.digits, 12))
            new_file_name = r'{0} - {1}{3}{2}'.format(
                song.song_name, ' & '.join(v.artist_name for v in song.artists), audio_ext, salt)
            file_path = os.path.join(self.tmp_folder, new_file_name)
        file_stream = open(file_path, 'wb+')
        file_ext = 'audio/{}'.format(audio_ext[1:])
        song = song
        return SongFile(file_name, file_path, file_url, file_stream, file_ext, song)
