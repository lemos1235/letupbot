import os
import string
from http.client import RemoteDisconnected
from random import choice, sample

import requests
from requests.exceptions import ConnectionError, Timeout, HTTPError
from telegram import InlineKeyboardButton, InlineKeyboardMarkup

from ducky.main import Main, Tele, Crawler
from ducky.music import SongListSelector, SongFile, Song, Album, Artist, SongList
from util import ButtonItem, slice_keywords, escape_keywords, Singleton, BotResult, conn_retry, NoCopyRight, \
    get_sources_btn, progress_download, md5_encrypt, userAgentList


class Kugou(Main):
    def __init__(self):
        super().__init__(OwnCrawler(), Util())


class OwnCrawler(Crawler, metaclass=Singleton):

    def __init__(self):
        super().__init__()
        self.headers = {
            'Accept': '*/*',
            'Accept-Encoding': 'gzip,deflate,sdch',
            'Accept-Language': 'zh-CN,zh;q=0.8,gl;q=0.6,zh-TW;q=0.4',
            'Connection': 'keep-alive',
            'Content-Type': 'application/x-www-form-urlencoded',
            'Referer': 'http://www.kugou.com/',
            'X-Real-IP': '59.111.160.197',
            'User-Agent': choice(userAgentList)
        }
        self.session = requests.session()
        self.session.headers.update(self.headers)
        self.download_session = requests.session()

    @staticmethod
    def dump_single_song(song, mode=0):
        if mode == 0:
            artist_list = [Artist(10010, song['singername'])]
            album_id, album_name = song['album_id'], song['album_name']
            album = Album(album_id, album_name)
            song_id = song.get('sqhash') or song.get('320hash') or song.get('128hash') or song.get('hash')
            # song_id = song.get('320hash') or song.get('128hash') or song.get('hash')
            song_id, song_name, song_duration, artists, album = song_id, song['songname'], song[
                'duration'], artist_list, album
            song = Song(song_id, song_name, song_duration, artists, album)
        if mode == 1:
            artist_list = []
            for ar in song['authors']:
                artist_id, artist_name = ar['author_id'], ar['author_name']
                artist_list.append(Artist(artist_id, artist_name))

            album_id, album_name = song['album_id'], song['album_name']
            album = Album(album_id, album_name)
            song_id = song['hash']
            song_id, song_name, song_duration, artists, album = song_id, song['song_name'], song[
                'timelength'], artist_list, album
            song = Song(song_id, song_name, song_duration, artists, album)
        return song

    @staticmethod
    def dump_songs(songs, mode=0):
        song_list = []
        for song in songs:
            song = OwnCrawler.dump_single_song(song, mode)
            song_list.append(song)
        return song_list

    def get_request(self, url, params=None, custom_session=None):
        """Send a get request.

        warning: old api.
        :return: a dict or raise Exception.
        """
        if not custom_session:
            resp = self.session.get(url, params=params, timeout=self.timeout,
                                    proxies=self.proxies)
        else:
            resp = custom_session.get(url, params=params, timeout=self.timeout,
                                      proxies=self.proxies)
        result = resp.json()
        if result.get('err_code') or result.get('errcode') or result.get('error'):
            self.logger.error('Return %s when try to get %s', result, url)
            raise ConnectionError(result)
        else:
            return result

    @conn_retry(exce=(Timeout, ConnectionError, RemoteDisconnected))
    def search_song(self, song_name, page=1, pagesize=5):
        url = "http://mobilecdn.kugou.com/api/v3/search/song"
        payload = {
            'keyword': song_name,
            'page': page,
            'pagesize': pagesize,
            'format': 'json',
            'showtype': 1
        }
        try:
            result = self.get_request(url, payload)
            if result['data']['total'] <= 0:
                self.logger.warning('Song %s not existed!', song_name)
                return BotResult(404, 'Song {} not existed.'.format(song_name))
            else:
                keyword, song_count, songs = song_name, result['data']['total'], result['data']['info']
                songlist = SongList(keyword, song_count, OwnCrawler.dump_songs(songs))
                return BotResult(200, body=songlist)
        except HTTPError as er:
            self.logger.error('Return {0} when try to get {1} => {2}'.format(er, url, payload), exc_info=True)
            return BotResult(400, "HTTPError")

    @conn_retry(exce=(Timeout, ConnectionError))
    def get_song_detail(self, song_id, quality=None):
        url = 'http://www.kugou.com/yy/index.php'
        payload = {
            'r': 'play/getdata',
            'hash': song_id
        }
        try:
            result = self.get_request(url, payload)
            song = result['data']
            single_song = OwnCrawler.dump_single_song(song, mode=1)
            # 获取 song_url
            url = self.get_song_url(song_id)
            single_song.song_url = url
            return BotResult(200, body=single_song)
        except NoCopyRight as e:
            return BotResult(401, "HTTPError, no copyright!")
        except HTTPError as er:
            self.logger.error('Return {0} when try to get {1} => {2}'.format(er, url, payload), exc_info=True)
            return BotResult(400, "HTTPError")

    @conn_retry(exce=(Timeout, ConnectionError))
    def get_song_url(self, song_id):
        url = "http://trackercdn.kugou.com/i/"
        payload = {
            'acceptMp3': 1,
            'cmd': 4,
            'pid': 6,
            'hash': song_id,
            'key': md5_encrypt(song_id + 'kgcloud')
        }
        result = self.get_request(url, payload)
        if result.get('error'):
            self.logger.warning(
                'Song %s is not available due to copyright issue. => %s',
                song_id, result)
            raise NoCopyRight('no copyright!')
        else:
            return result['url']

    def write_file(self, songfile):
        progress_download(self.download_session, songfile)


class Util(Tele, metaclass=Singleton):
    def __init__(self):
        super().__init__()

    def get_songlist_selector(self, curpage, songlist):
        """
                生成可供选择的歌曲选择器
                :param curpage: 当前页数
                :param songlist: 歌曲列表🎵
                :return: 歌曲列表选择器
                """
        total_page = (songlist.track_count + 4) // 5
        title = 'დ  Search  ‹ {0} ›  →  {1}/{2}'.format(songlist.keyword, curpage, total_page)
        return SongListSelector(title, curpage, total_page, songlist)

    def produce_songlist_panel(self, module_name, songlist_selector):
        button_list = []
        # 由于 tg 对 callback_data 字数限制，必须对关键词进行切片
        songlist_selector.songlist.keyword = slice_keywords(songlist_selector.songlist.keyword)

        for x in songlist_selector.songlist.songs:
            time_fmt = '{0}:{1:0>2d}'.format(int(x.song_duration // 60), int(x.song_duration % 60))
            button_list.append([
                InlineKeyboardButton(
                    text='{1} - {2}'.format(
                        time_fmt, x.song_name, ' / '.join(v.artist_name for v in x.artists)),
                    callback_data="{0}:{1}:{2}:{3}:{4}".format(module_name, ButtonItem.TYPE_SONGLIST,
                                                               ButtonItem.OPERATE_SEND,
                                                               x.song_id, 0)
                )
            ])

        button_list.append(get_sources_btn())

        if songlist_selector.total_page == 1:
            pass
        elif songlist_selector.cur_page == 1:
            button_list.append([
                InlineKeyboardButton(
                    text='»',
                    callback_data="{0}:{1}:{2}:{3}:{4}".format(module_name, ButtonItem.TYPE_SONGLIST,
                                                               ButtonItem.OPERATE_PAGE_DOWN,
                                                               songlist_selector.songlist.keyword,
                                                               songlist_selector.cur_page)
                )
            ])
        elif songlist_selector.cur_page == songlist_selector.total_page:
            button_list.append([
                InlineKeyboardButton(
                    text='«',
                    callback_data="{0}:{1}:{2}:{3}:{4}".format(module_name, ButtonItem.TYPE_SONGLIST,
                                                               ButtonItem.OPERATE_PAGE_UP,
                                                               songlist_selector.songlist.keyword,
                                                               songlist_selector.cur_page)
                )
            ])
        else:
            button_list.append([
                InlineKeyboardButton(
                    text='«',
                    callback_data="{0}:{1}:{2}:{3}:{4}".format(module_name, ButtonItem.TYPE_SONGLIST,
                                                               ButtonItem.OPERATE_PAGE_UP,
                                                               songlist_selector.songlist.keyword,
                                                               songlist_selector.cur_page)
                ),
                InlineKeyboardButton(
                    text='»',
                    callback_data="{0}:{1}:{2}:{3}:{4}".format(module_name, ButtonItem.TYPE_SONGLIST,
                                                               ButtonItem.OPERATE_PAGE_DOWN,
                                                               songlist_selector.songlist.keyword,
                                                               songlist_selector.cur_page)
                )
            ])

        return {'text': songlist_selector.title, 'reply_markup': InlineKeyboardMarkup(button_list)}

    def get_songfile(self, song):
        song.song_name = slice_keywords(song.song_name, 200, 10)  # 对文件名长度限制一般性处理
        audio_ext = os.path.splitext(song.song_url)[1]
        file_name = r'{0} - {1}{2}'.format(
            song.song_name, ' & '.join(v.artist_name for v in song.artists), audio_ext)
        file_name = escape_keywords(file_name, '/', ':')
        file_path = os.path.join(self.tmp_folder, file_name)
        file_url = song.song_url
        if os.path.exists(file_path):
            salt = ''.join(sample(string.ascii_letters + string.digits, 12))
            new_file_name = r'{0} - {1}{3}{2}'.format(
                song.song_name, ' & '.join(v.artist_name for v in song.artists), audio_ext, salt)
            file_path = os.path.join(self.tmp_folder, new_file_name)
        file_stream = open(file_path, 'wb+')
        file_ext = 'audio/{}'.format(audio_ext[1:])
        song = song
        return SongFile(file_name, file_path, file_url, file_stream, file_ext, song)
