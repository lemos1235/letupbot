# LetupBot

### python3 模块

```
python-telegram-bot
requests
redis
pycryptodome
mutagen
```

安装依赖模块
```
pip install --no-cache-dir -r requirements.txt
```

### 启动项目

```bash
python3 poll.py
```
