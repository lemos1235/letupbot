import logging
import os
import sys

from telegram.error import (TelegramError, Unauthorized, BadRequest,
                            TimedOut, ChatMigrated, NetworkError)
from telegram.ext import Updater

import dispatcher
import util


class Bot(object):
    def __init__(self):
        self.setup_logger()
        self.logger = logging.getLogger("__name__")
        self.control = dispatcher.Dispatcher()

    @staticmethod
    def setup_logger():
        logging.basicConfig(
            format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
            level=logging.WARNING)

    def error_callback(self, bot, update, error):
        try:
            raise error
        except (BadRequest, TimedOut) as er:
            self.logger.warning('Update %s caused error %s', update, er)
        except Unauthorized:
            self.logger.warning('Update %s caused error Unauthorized', update)
        except ChatMigrated:
            self.logger.warning('Update %s caused error', update)
        except NetworkError:
            self.logger.warning('Update %s caused error', update)
        except TelegramError as err:
            self.logger.warning('Update %s caused error %s', update, err)

    def pull_data(self, signum, frame):
        util.SessionBase().uduration()
        self.logger.debug("SessionsBase have persisted!")

    def start_bot(self, own_token=None):
        if own_token:
            updater = Updater(token=own_token, workers=7)
        else:
            updater = Updater(token=util.load_config()['bot_token'], workers=7, user_sig_handler=self.pull_data)

        self.control.handler_response(updater.dispatcher)
        updater.dispatcher.add_error_handler(self.error_callback)

        updater.start_polling(poll_interval=0.0, timeout=0, clean=True, read_latency=4)
        updater.idle()


if __name__ == '__main__':
    if len(sys.argv) == 2 and sys.argv[1] == "--devel":
        os.environ['HTTP_PROXY'] = "http://127.0.0.1:8118"
        os.environ['HTTPS_PROXY'] = "http://127.0.0.1:8118"
        os.environ['NO_PROXY'] = "m1.musics.126.net,10.*.*.*,192.168.*.*,*.local,localhost,127.0.0.1"
        token = "454178729:AAEo7iBT4MvVN2T3EK8MyZTiLdj2ljlQDO4"
        Bot().start_bot(token)
    else:
        Bot().start_bot()
