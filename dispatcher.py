import logging
import re

from telegram import ChatMember, InlineKeyboardMarkup, InlineKeyboardButton, ParseMode
from telegram.ext import CommandHandler, CallbackQueryHandler, InlineQueryHandler, run_async, MessageHandler, \
    RegexHandler
from telegram.ext.filters import Filters

import bauble
from ducky import animation, dog, pig, penguin
from util import SessionBase


class Dispatcher(object):
    def __init__(self):
        self.logger = logging.getLogger(__name__)

        self.netease = pig.Netease()
        self.kugou = dog.Kugou()
        self.tencent = penguin.Tencent()
        self.animation = animation.Animation()

        self.inline = bauble.Inline()
        self.settings = bauble.Settings()
        self.session_base = SessionBase()

    def start_prompt(self, bot, update, args):
        if args:
            params = args[0].split('-')
            if params[0] == 'get_music':
                module_id = params[1]
                song_id = params[2]
                if module_id == self.netease.unique_id:
                    self.netease.deliver_music(bot, update, song_id, delete=False)
                elif module_id == self.kugou.unique_id:
                    self.kugou.deliver_music(bot, update, song_id, delete=False)
                elif module_id == self.tencent.unique_id:
                    self.tencent.deliver_music(bot, update, song_id, delete=False)
            elif params[0] == 'new_song':
                self.netease.find_personalfm(bot, update)
        else:
            bot.send_message(chat_id=update.message.chat.id, text="{}, いらっしゃいませ.".format(update.message.from_user.first_name))

    def starter_main_menu(self, bot, update, args):
        self.settings.starter_menu_main(bot, update)

    def starter_callback(self, bot, update):
        self.settings.dispatch_callback(bot, update)

    def config_source(self, bot, update, args):
        source_id = args[0]
        if source_id in ["1", "2", "3"]:
            if source_id == "1":
                self.session_base.uset(update.message.from_user.id, 'module_c', 'netease_c')
            elif source_id == "2":
                self.session_base.uset(update.message.from_user.id, 'module_c', 'kugou_c')
            elif source_id == "3":
                self.session_base.uset(update.message.from_user.id, 'module_c', 'tencent_c')

    @run_async
    def source_toggle(self, bot, query):
        kw = re.search(r"Search\s\s‹\s(.*)\s›\s\s→", query.message.text).group(1)
        _source = query.data.split(':')[1]
        if _source == "netease_c":
            self.netease.songlist_turning(bot, query, kw)
        elif _source == "kugou_c":
            self.kugou.songlist_turning(bot, query, kw)
        elif _source == "tencent_c":
            self.tencent.songlist_turning(bot, query, kw)

    @run_async
    def music_callback(self, bot, update):
        query = update.callback_query
        if query.data[:6] == "source":
            self.source_toggle(bot, query)
        else:
            param = query.data
            if param.startswith('netease_c'):
                self.netease.analyze_music_callback(bot, update)
            elif param.startswith('kugou_c'):
                self.kugou.analyze_music_callback(bot, update)
            elif param.startswith('tencent_c'):
                self.tencent.analyze_music_callback(bot, update)

    @run_async
    def inline_query(self, bot, update):
        self.inline.search(bot, update)

    @run_async
    def module_analyze(self, bot, update):
        module_id = self.session_base.uget(update.message.from_user.id, 'module_c')
        if update.message.text and '-' not in update.message.text and '›' not in update.message.text:
            if module_id:
                if module_id == self.netease.unique_id:
                    self.netease.find_music(bot, update, update.message.text)
                elif module_id == self.kugou.unique_id:
                    self.kugou.find_music(bot, update, update.message.text)
                elif module_id == self.tencent.unique_id:
                    self.tencent.find_music(bot, update, update.message.text)
            else:
                self.netease.find_music(bot, update, update.message.text)

    def netease_match(self, bot, update):
        if update.message.chat.type == "private" and re.match(
                r'.*https?://music.163.com/?#?/?m?/playlist((/)|(\?id=))(\d*).*', update.message.text):
            playlist_id = re.search(r'https?://music.163.com/?#?/?m?/playlist((/)|(\?id=))(\d*)',
                                    update.message.text).group(4)
            self.netease.find_playlist(bot, update, playlist_id)

    @run_async
    def group_analyze(self, bot, update):
        if update.message and update.message.text and '-' not in update.message.text:
            admin_list = bot.get_chat_administrators(update.message.chat.id)
            public = None
            for x in admin_list:
                if x.status == ChatMember.CREATOR:
                    public = self.session_base.uget(x.user.id, 'public_c')
            if public and public == 'allow':
                if re.match(r'.*(我要听|我想听|来一首|来一曲|推荐一首).*', update.message.text):
                    song_name = "最新音乐"
                    if re.search(r'(我要听|我想听|来一首|来一曲|推荐一首)\s*([!,！].+)',
                                 update.message.text):
                        song_name = re.search(r'(我要听|我想听|来一首|来一曲|推荐一首)\s*([!,！].+)',
                                              update.message.text).group(2)[1:]

                    elif re.search(r'(我要听|我想听|来一首|来一曲|推荐一首)(.*的)?(.+)',
                                   update.message.text):
                        result = re.search(r'(我要听|我想听|来一首|来一曲|推荐一首)(.*的)?(.+)',
                                           update.message.text).groups()

                        if result[1]:
                            song_name = result[2] + ' ' + result[1][:-1]
                        else:
                            song_name = result[2]

                    self.netease.group_music_search(bot, update, song_name)

                elif re.match(r'.*https?://music.163.com/?#?/?m?/song((/)|(\?id=))(\d*).*', update.message.text):
                    song_id = re.search(r'https?://music.163.com/?#?/?m?/song((/)|(\?id=))(\d*)',
                                        update.message.text).group(4)
                    self.netease.group_single_music(bot, update, song_id)
                elif re.match(r'.*https?://y.qq.com/.*/song((/)|(\?id=))(\w*).*?', update.message.text):
                    song_id = re.search(r'https?://y.qq.com/.*/song((/)|(\?id=))(\w*)',
                                        update.message.text).group(4)
                    self.tencent.group_single_music(bot, update, song_id)
                elif re.match(r'.*https?://www.kugou.com/.*#hash=(\w*).*?', update.message.text):
                    song_id = re.search(r'https?://www.kugou.com/.*#hash=(\w*)',
                                        update.message.text).group(1)
                    self.kugou.group_single_music(bot, update, song_id)

        update.message = update.edited_message if update.edited_message else update.message
        if update.message.caption:
            if 'anime' in update.message.caption or '查看动画' in update.message.caption:
                if update.message.photo or update.message.sticker or (
                        update.message.document and update.message.document.thumb):
                    self.animation.search_animation(bot, update)

    @staticmethod
    def help_info(bot, update):
        if update.message.chat.type == "private":
            text = "Developer Info!"
            markup = InlineKeyboardMarkup(
                [[InlineKeyboardButton(text="Open Github", url="https://github.com/lemos1235"),
                  InlineKeyboardButton(text="Rating",
                                       url="https://telegram.me/storebot?start=leoebot")]])
            bot.send_message(chat_id=update.message.chat.id,
                             text=text,
                             disable_web_page_preview=True,
                             disable_notification=True,
                             parse_mode=ParseMode.MARKDOWN,
                             reply_markup=markup)

    def handler_response(self, dispatcher):
        dispatcher.add_handler(CommandHandler(['start'], self.start_prompt, pass_args=True, filters=Filters.private),
                               group=5)

        dispatcher.add_handler(
            CommandHandler(['s', 'source'], self.config_source, pass_args=True, filters=Filters.private), group=5)

        dispatcher.add_handler(
            CommandHandler(['settings', 'config'], self.starter_main_menu, pass_args=True, filters=Filters.private),
            group=5)

        dispatcher.add_handler(
            CallbackQueryHandler(self.starter_callback, pattern=r"(home|settings|quality|public)"), group=5)

        dispatcher.add_handler(RegexHandler(r".*https?://music.163.com.*", self.netease_match), group=4)

        dispatcher.add_handler(
            CallbackQueryHandler(self.music_callback, pattern=r"(netease_c|kugou_c|tencent_c|source)"), group=3)

        dispatcher.add_handler(
            MessageHandler(~ Filters.command & Filters.group, self.group_analyze, edited_updates=True), group=2)

        dispatcher.add_handler(MessageHandler(Filters.text & Filters.private, self.module_analyze), group=1)

        dispatcher.add_handler(InlineQueryHandler(self.inline_query), group=1)

        dispatcher.add_handler(RegexHandler("/?help", self.help_info), group=5)
