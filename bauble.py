import logging
from uuid import uuid4

from telegram import InlineQueryResultArticle, InlineQueryResultCachedDocument, InputTextMessageContent, \
    InlineKeyboardMarkup, InlineKeyboardButton, ParseMode

from ducky import dog, pig, penguin
from util import DataCache, telegram_error, SessionBase, telegram_timeout


class Inline(object):
    bot_username = "leoebot"

    def __init__(self):
        self.logger = logging.getLogger(__name__)

    @telegram_error
    def search(self, bot, update):
        my_query = update.inline_query
        query_param = my_query.query

        if not Inline.bot_username:
            Inline.bot_username = bot.get_me().username

        module_id = SessionBase().uget(my_query.from_user.id, 'module_c')

        if module_id == 'kugou_c':
            custom_crawler = dog.OwnCrawler()
        elif module_id == 'tencent_c':
            custom_crawler = penguin.OwnCrawler()
        else:
            module_id = 'netease_c'
            custom_crawler = pig.OwnCrawler()

        if my_query.offset == '':
            this_offset = 1
        else:
            this_offset = int(my_query.offset)

        if query_param:
            this_results = []

            bot_result = custom_crawler.search_song(query_param, page=this_offset, pagesize=3)
            if bot_result.status == 200:
                for song in bot_result.get_body().songs:
                    ats = ' / '.join(v.artist_name for v in song.artists)
                    song_text = '{0} - {1}'.format(song.song_name, ats)
                    message_content = InputTextMessageContent(message_text=song_text, disable_web_page_preview=True)
                    link_url = 'https://telegram.me/{0}?start={1}'.format(Inline.bot_username,
                                                                          'get_music-{0}-{1}'.format(module_id,
                                                                                                     song.song_id))

                    # Judge whether the musics cache exist?
                    _file_id = DataCache().song_get(module_id, song.song_id)
                    if _file_id:
                        this_results.append(InlineQueryResultCachedDocument(id=uuid4(), title=song.song_name,
                                                                            description=ats,
                                                                            document_file_id=_file_id,
                                                                            caption="[Enter Bot](https://telegram.me/{}".format(
                                                                                Inline.bot_username),
                                                                            parse_mode=ParseMode.MARKDOWN))
                    else:
                        action_markup = InlineKeyboardMarkup([[InlineKeyboardButton(text="◕ฺ‿◕ฺ✿ฺ)", url=link_url)]])
                        this_results.append(InlineQueryResultArticle(id=uuid4(), title=song.song_name,
                                                                     input_message_content=message_content,
                                                                     description=' / '.join(
                                                                         v.artist_name for v in song.artists),
                                                                     thumb_url=song.album.album_pic_url,
                                                                     reply_markup=action_markup))
                this_next_offset = this_offset + 1
                my_query.answer(this_results, cache_time=0, next_offset=str(this_next_offset), timeout=55)

        else:
            my_query.answer([], cache_time=5, switch_pm_text="NEW SONG", switch_pm_parameter="new_song")


class Settings(object):
    def __init__(self):
        self.session_base = SessionBase()

    @staticmethod
    def quality_panel(quality):
        if quality and quality == 'highest':
            msg_text = 'Highest'
        else:
            msg_text = 'Medium'
        button_list = [
            [InlineKeyboardButton(text='Medium', callback_data='quality' + ':medium'),
             InlineKeyboardButton(text='Highest', callback_data='quality' + ':highest')],
            [InlineKeyboardButton(text='↩️', callback_data='home' + ':return_home')]
        ]
        markup = InlineKeyboardMarkup(button_list)
        return msg_text, markup

    @staticmethod
    def public_panel(public):
        if public and public == 'allow':
            msg_text = 'Allow'
        else:
            msg_text = 'Reject'
        button_list = [
            [InlineKeyboardButton(text='Allow', callback_data='public' + ':allow'),
             InlineKeyboardButton(text='Reject', callback_data='public' + ':disallow')],
            [InlineKeyboardButton(text='↩️', callback_data='home' + ':return_home')]
        ]
        markup = InlineKeyboardMarkup(button_list)
        return msg_text, markup

    @telegram_timeout
    @telegram_error
    def starter_menu_main(self, bot, update, go_back=False):
        msg_text = '<)。(>'
        button_list = [
            [InlineKeyboardButton(text='Quality', callback_data='home' + ':quality'),
             InlineKeyboardButton(text='Group?', callback_data='home' + ':public')]
        ]
        markup = InlineKeyboardMarkup(button_list)
        if go_back:
            update.message.edit_text(text=msg_text, reply_markup=markup, parse_mode=ParseMode.MARKDOWN)
        else:
            bot.send_message(chat_id=update.message.chat.id, text=msg_text, reply_markup=markup,
                             parse_mode=ParseMode.MARKDOWN)

    @telegram_timeout
    @telegram_error
    def dispatch_callback(self, bot, update):
        query = update.callback_query
        params = query.data.split(':')
        if params[0] == 'home':
            if params[1] == 'quality':
                self.show_quality(bot, query)
            elif params[1] == 'public':
                self.show_public(bot, query)
            elif params[1] == 'return_home':
                self.starter_menu_main(bot, query, go_back=True)
        elif params[0] == 'quality':
            self.session_base.uset(query.message.chat.id, 'quality_c', params[1])
            p = self.quality_panel(params[1])
            query.message.edit_text(text=p[0], reply_markup=p[1])
        elif params[0] == 'public':
            # TODO special group only
            self.session_base.uset(query.message.chat.id, 'public_c', params[1])
            p = self.public_panel(params[1])
            query.message.edit_text(text=p[0], reply_markup=p[1])

    @telegram_timeout
    @telegram_error
    def show_quality(self, bot, query):
        quality = self.session_base.uget(query.message.chat.id, 'quality_c')
        panel = self.quality_panel(quality)
        query.message.edit_text(text=panel[0], reply_markup=panel[1])

    @telegram_timeout
    @telegram_error
    def show_public(self, bot, query):
        public = self.session_base.uget(query.message.chat.id, 'public_c')
        panel = self.public_panel(public)
        query.message.edit_text(text=panel[0], reply_markup=panel[1])
