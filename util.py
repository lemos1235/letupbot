import base64
import binascii
import hashlib
import json
import logging
import os
from functools import wraps
from threading import Event

import redis
from Crypto.Cipher import AES
from requests.exceptions import ConnectionError, Timeout, ReadTimeout
from requests.exceptions import HTTPError
from telegram import InlineKeyboardButton
from telegram.error import (BadRequest, TimedOut, ChatMigrated)


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


def load_config():
    with open("config.json", 'r') as f:
        cfg = json.load(f)
        return cfg


def telegram_error(func):
    logger = logging.getLogger('telegram_error')

    @wraps(func)
    def wrapped(*args, **kwargs):
        try:
            result = func(*args, **kwargs)
            return result
        except (BadRequest, ChatMigrated) as es:
            logger.warning('Telegram error %s', es)

    return wrapped


def telegram_timeout(func):
    logger = logging.getLogger('telegram_timeout')
    retry, delay = 2, 1

    @wraps(func)
    def wrapped(*args, **kwargs):
        nonlocal logger, retry, delay
        try:
            logger.debug('Telegram retry, current max_retries=%s', retry)
            result = func(*args, **kwargs)
            return result
        except TimedOut as err:
            while retry > 0:
                try:
                    return func(*args, **kwargs)
                except TimedOut:
                    pass
                finally:
                    retry = retry - 1
                    delay = delay + 2
                    Event().wait(delay)
            else:
                logger.warning('Telegram TimedOut error')

    return wrapped


def conn_retry(max_retries=4, delay=0.3, step=0.4, exce=BaseException):
    """
    Handle the Http connect timeout, the 'exce' is usually Timeout, ConnectError.
    Don't set some special exception into exce that we can't solve the exception.
    """
    logger = logging.getLogger('conn_retry')

    def wrapper(func):
        @wraps(func)
        def wrap(*args, **kwargs):
            nonlocal logger, delay, step, max_retries, exce
            try:
                result = func(*args, **kwargs)
                return result
            except exce as err:
                while max_retries > 0:
                    try:
                        logger.debug('Connect retry, current max_retries=%s', max_retries)
                        return func(*args, **kwargs)
                    except exce as e:
                        err = e
                    finally:
                        max_retries -= 1
                        if delay > 0 or step > 0:
                            delay += step
                            Event().wait(delay)
                else:
                    try:
                        raise err
                    except UrlUpdateError as eu:
                        return str(eu)
                    except BaseException:
                        return BotResult(400, 'Connect error: {}'.format(err))

        return wrap

    return wrapper


def selector_cancel(bot, query):
    try:
        bot.answerCallbackQuery(query.id, text="loading..", show_alert=False)
        if query.message:
            query.message.delete()
    except BadRequest as e:
        pass


def get_sources_btn():
    button_group = [InlineKeyboardButton(text='1', callback_data='source' + ':netease_c'),
                    InlineKeyboardButton(text='2', callback_data='source' + ':kugou_c'),
                    InlineKeyboardButton(text='3', callback_data='source' + ':tencent_c')]
    return button_group


def slice_keywords(kw, maxlen=48, step=3):
    """
    U can set step = 1, to get the precise result!
    """
    num = 1
    cut_kw = kw
    while isinstance(kw, str) and len(cut_kw.encode('utf-8')) > maxlen:
        cut = len(kw.encode('utf-8')) - step * num
        cut_kw = kw[:cut]
        num += 1
    return cut_kw


def escape_keywords(kw, character='_', replace=' '):
    for c in character:
        kw = kw.replace(c, replace)
    return kw


def is_contain_zh(content):
    for x in content:
        return u'\u4e00' <= x <= u'\u9fa5'


class NoCopyRight(HTTPError):
    def __repr__(self):
        return 'NoCopyRight Exception'


class UrlUpdateError(HTTPError):
    def __repr__(self):
        return 'UrlUpdateError Exception'


class UnHandleError(Exception):
    def __repr__(self):
        return 'UnHandleError Exception'


class ButtonItem(object):
    TYPE_SONGLIST = 1
    TYPE_PLAYLIST = 2
    TYPE_TOPLIST = 3
    TYPE_DIALOG = 4
    TYPE_TOPLIST_CATEGORY = 5
    TYPE_PERSONAL_FM = 6

    OPERATE_CANCEL = '*'
    OPERATE_PAGE_DOWN = '+'
    OPERATE_PAGE_UP = '-'
    OPERATE_SEND = '#'

    OPERATE_ENTER = '+'
    OPERATE_EXIT = '-'

    def __init__(self, pattern, button_type, button_operate, item_id="", page=0):
        """
        由于 64 字节限制。故采用变量缩写。
        :param pattern: query_data 匹配的模块
        :param button_type: 按钮的类型，TYPE_SONGLIST， TYPE_PLAYLIST， TYPE_TOPLIST
        :param button_operate: 按钮的操作， OPERATE_PAGE_DOWN， OPERATE_PAGE_UP， OPERATE_CANCEL
        :param item_id: 项目 id
        :param page: 项目当前页数
        """
        self.pattern = pattern
        self.button_type = button_type
        self.button_operate = button_operate
        self.item_id = item_id
        self.page = page

    @classmethod
    def parse_data(cls, data):
        result = data.split(":")
        pattern, button_type, operate, item_id, page = \
            result[0], result[1], result[2], result[3], result[4]
        button_item = cls(pattern, int(button_type), operate, item_id, int(page))
        return button_item


class BotResult:
    def __init__(self, code, msg='', body=None):
        self.code = code
        self.msg = msg
        self.body = body

    @property
    def status(self):
        return self.code

    def get_status(self):
        return self.code

    def get_msg(self):
        return self.msg

    def get_body(self):
        return self.body


class SessionBase(object):
    _base = dict()

    def __init__(self):
        self.dcache = DataCache()

    def get(self, key):
        return self._base.get(key)

    def set(self, key, val):
        self._base[key] = val

    def delete(self, key):
        del self._base[key]

    def contain(self, key):
        return key in self._base

    def uget(self, user_id, user_label):
        _key = '{0}:{1}'.format(user_id, user_label)
        if _key in self._base:
            return self._base.get(_key)
        else:
            result = self.dcache.user_get(user_id, user_label)
            return result

    def uset(self, user_id, user_label, val):
        _key = '{0}:{1}'.format(user_id, user_label)
        self._base[_key] = val

    def fm_get(self, chat_id):
        return self.uget(chat_id, 'fm')

    def fm_set(self, chat_id, val):
        self.uset(chat_id, 'fm', val)

    def uduration(self):
        for key, val in self._base.items():
            lst = key.split(':', 1)
            user_id, user_label = lst[0], lst[1]
            self.dcache.user_set(user_id, user_label, val)


class DataCache(object):
    def __init__(self):
        self.pool = redis.ConnectionPool(host=load_config()['database']['redis_host'],
                                         port=load_config()['database']['redis_port'], db=0,
                                         password=load_config()['database']['redis_pwd'], retry_on_timeout=True,
                                         decode_responses=True)

    def get_conn(self):
        _conn = redis.Redis(connection_pool=self.pool)
        return _conn

    def set(self, key, val):
        self.get_conn().set(key, val)

    def delete(self, key):
        self.get_conn().delete(key)

    def user_get(self, user_id, user_label):
        _result = self.get_conn().hget(user_label, user_id)
        return _result

    def user_set(self, user_id, user_label, val):
        self.get_conn().hset(user_label, user_id, val)

    def user_delete(self, user_id, user_label):
        self.get_conn().hdel(user_label, user_id)

    def song_get(self, mode_name, song_id):
        song_label = '{0}/{1}'.format('music', mode_name)
        _result = self.user_get(song_id, song_label)
        return _result

    def song_set(self, mode_name, song_id, val):
        song_label = '{0}/{1}'.format('music', mode_name)
        self.user_set(song_id, song_label, val)

    def song_delete(self, mode_name, song_id):
        song_label = '{0}/{1}'.format('music', mode_name)
        self.user_delete(song_id, song_label)


userAgentList = [
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
    'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1',
    'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1',
    'Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 5.1.1; Nexus 6 Build/LYZ28E) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Mobile Safari/537.36',
    'Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_2 like Mac OS X) AppleWebKit/603.2.4 (KHTML, like Gecko) Mobile/14F89;GameHelper',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/603.2.4 (KHTML, like Gecko) Version/10.1.1 Safari/603.2.4',
    'Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac OS X) AppleWebKit/602.1.38 (KHTML, like Gecko) Version/10.0 Mobile/14A300 Safari/602.1',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:46.0) Gecko/20100101 Firefox/46.0',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:46.0) Gecko/20100101 Firefox/46.0',
    'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)',
    'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0)',
    'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)',
    'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Win64; x64; Trident/6.0)',
    'Mozilla/5.0 (Windows NT 6.3; Win64, x64; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/13.10586',
    'Mozilla/5.0 (iPad; CPU OS 10_0 like Mac OS X) AppleWebKit/602.1.38 (KHTML, like Gecko) Version/10.0 Mobile/14A300 Safari/602.1'
]

MODULUS = '00e0b509f6259df8642dbc35662901477df22677ec152b5ff68ace615bb7b725152b3ab17a876aea8a5aa76d2e417629ec4ee341f56135fccf695280104e0312ecbda92557c93870114af6c9d05c4f7f0c3685b7a46bee255932575cce10b424d813cfe4875d3e82047b97ddef52741d546b8e289dc6935b3ece0462db0a22b8e7'

NONCE = '0CoJUm6Qyw8W8jud'

PUB_KEY = '010001'


def encrypted_request(text):
    text = json.dumps(text)
    sec_key = create_secret_key(16)
    enc_text = aes_encrypt(aes_encrypt(text, NONCE), sec_key.decode('utf-8'))
    enc_sec_key = rsa_encrypt(sec_key, PUB_KEY, MODULUS)
    data = {'params': enc_text, 'encSecKey': enc_sec_key}
    return data


def aes_encrypt(text, secKey):
    pad = 16 - len(text) % 16
    text = text + chr(pad) * pad
    encryptor = AES.new(secKey.encode('utf-8'), AES.MODE_CBC, b'0102030405060708')
    ciphertext = encryptor.encrypt(text.encode('utf-8'))
    ciphertext = base64.b64encode(ciphertext).decode('utf-8')
    return ciphertext


def rsa_encrypt(text, pubKey, modulus):
    text = text[::-1]
    rs = pow(int(binascii.hexlify(text), 16), int(pubKey, 16), int(modulus, 16))
    return format(rs, 'x').zfill(256)


def create_secret_key(size):
    return binascii.hexlify(os.urandom(size))[:16]


def md5_encrypt(sh):
    md5 = hashlib.md5()
    md5.update(sh.encode())
    return md5.hexdigest()


@conn_retry(exce=(Timeout, ConnectionError, ReadTimeout))
def progress_download(session, songfile):
    with session.get(songfile.file_url, stream=True, timeout=7) as r:
        songfile.file_stream.write(r.content)
        songfile.file_stream.close()
